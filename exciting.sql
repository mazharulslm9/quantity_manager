-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2015 at 06:43 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exciting`
--

-- --------------------------------------------------------

--
-- Table structure for table `quantity_tbl`
--

CREATE TABLE IF NOT EXISTS `quantity_tbl` (
`id` int(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `manager` varchar(255) NOT NULL,
  `number` int(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `quantity_tbl`
--

INSERT INTO `quantity_tbl` (`id`, `day`, `date`, `manager`, `number`) VALUES
(1, 'wed', '2015-12-01', 'jaman', 50),
(2, 'wed', '2015-05-10', 'jaman', 70),
(3, 'mon', '2013-08-08', 'rahad', 10),
(4, 'sun', '2015-05-10', 'jaman', 100),
(6, 'sat', '2015-12-30', 'maria', 10),
(7, 'tues', '2014-10-10', 'siam', 100),
(8, 'wed', '2015-05-10', 'jaman', 10),
(9, 'wed', '2015-05-10', 'jaman', 20),
(11, 'SUN', '2013-02-02', 'Rafiq', 100),
(12, 'Sat', '2012-02-23', 'Faruk', 20),
(13, 'SUN', '2013-02-02', 'rahad', 100),
(14, 'Sat', '2014-10-10', 'jaman', 100),
(17, 'Sat', '2015-05-10', 'Faruk', 80),
(18, 'WED', '2012-02-23', 'Mazhar', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quantity_tbl`
--
ALTER TABLE `quantity_tbl`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quantity_tbl`
--
ALTER TABLE `quantity_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
