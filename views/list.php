<?php
include_once '../vendor/autoload.php';

use App\Mazharul\Table\Table;

$quantity = new Table();
$qtm = $quantity->index();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Quantity Manager</title>

        <!-- Bootstrap -->
        <link href="../resource/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     
        <![endif]-->
        <style>
            table{
                width: 500px;
                border: 1px solid black;
         background-color: #e2e2e2;
                border-collapse: collapse;
                text-align: left;
            }

            table,thead {
                border-bottom:1px solid black;
            }
            table, tfoot {
                border-top: 1px solid black;
            }
            table,td,th {
                border-right: 1px solid black;
                  border-bottom:1px solid black;
                text-align: center;
            }
            th,td {
                padding: 5px;
            }
        </style>
    </head>
    <body class="bg-warning">
        <section>
            <div class="container">
                <div style="padding: 20px;" class="row">
                    <table>
                        <button style="margin: 15px 0px;" class="btn btn-md btn-warning"><a class="text-primary" href="../index.php">Add data</a></button> 


                        <thead>
                            <tr>
                                <th>Weekday</th>
                                <th>Date</th>
                                <th>Manager</th>
                                <th>Qty</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($qtm as $qt) {
                                ?>
                                <tr>
                                    <td class="text-uppercase"><?php echo $qt->day; ?></td>  
                                    <td><?php echo date('d-m-Y', strtotime($qt->date)) ?></td>  
                                    <td class="text-uppercase"><?php echo $qt->manager; ?></td>  
                                    <td><?php echo $qt->number; ?></td>  


                                </tr>

                                <?php
                            }
                            ?>



                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-uppercase" colspan="3">
                                    Total
                                </td>
                                <td>
                                    <?php
                                    echo $sumall = $quantity->sum();
                                    ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>

            </div>

        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../resource/js/bootstrap.min.js"></script>
    </body>
</html>